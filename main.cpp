/*
 * Main.cpp
 *
 *  Created on: Fall 2019
 */

#include <stdio.h>
#include <immintrin.h> // Required to use intrinsic functions
#include <CImg.h>
#include <malloc.h>
using namespace cimg_library;
#include <iostream>
using namespace std;
// Packet size used is: 256
#define ITEMS_PER_PACKET (sizeof(__m256)/sizeof(float))

// Data type for image components
typedef float data_t;

/* Function to be used for overlaping two images */
struct ColorPointer applyOverlap(ColorPointer srcImg, ColorPointer srcImg2,int numberOfPackets,ColorPointer destImg);

/* Function to check for overflows */
data_t checkOverflow(data_t toCheck);

const char* SOURCE_IMG      = "bailarina(2).bmp";
const char* SOURCE_IMG2 	= "background_V(2).bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";
const int SAMPLES = 35;

/* Axuiliar struct containing possible images pointers */
struct ColorPointer
{
    data_t *red;
    data_t *green;
    data_t *blue;
};

int main() {
    std::cout << ITEMS_PER_PACKET << "\n";
	// Variables for time measuring
	struct timespec tStart, tEnd;
	double dElapsedTimeS;

    // Open file and object initialization
	CImg<data_t> srcImage(SOURCE_IMG);
	CImg<data_t> srcImage2(SOURCE_IMG2);

    data_t *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	data_t *pRsrc2, *pGsrc2, *pBsrc2; // Pointers to the R, G and B components of second image
	data_t *pRdest, *pGdest, *pBdest;

    data_t *pDstImage; // Pointer to the new image pixels
    int width, height, width2, height2; // Width and height of the image
	uint nComp; // Number of image components

    /* Variables for handling both source images */
    //srcImage.display();       // Show the source image
    width = srcImage.width(); // Getting information from the source image
    height = srcImage.height();
    nComp = srcImage.spectrum(); // Source image number of components
    // Common values for spectrum (number of image components):
    // B&W images = 1
    // Normal color images = 3 (RGB)
    // Special color images = 4 (RGB and alpha/transparency channel)

    //srcImage2.display(); // Displays the second source image
	width2  = srcImage2.width(); // Getting information from the second source image
	height2 = srcImage2.height();

    if(width != width2 || height != height2) {
		perror("Both images must have the same width and height\n");
		exit(-1);
	}

	// Size of the image
    int imageSize = width * height;

    // Allocate memory space for the pixels of the destination (processed) image
    pDstImage = (data_t *)malloc(width * height * nComp * sizeof(data_t));
    if (pDstImage == NULL)
    {
        printf("\nMemory allocating error\n");
        exit(-2);
    }

    // Pointers to the componet arrays of the source image
	pRsrc = srcImage.data(); // pRcomp points to the R component array
	pGsrc = pRsrc + height * width; // pGcomp points to the G component array
	pBsrc = pGsrc + height * width; // pBcomp points to B component array

	// Pointers to the componet arrays of the second source image
	pRsrc2 = srcImage2.data(); // pRcomp points to the R component array
	pGsrc2 = pRsrc2 + height2 * width2; // pGcomp points to the G component array
	pBsrc2 = pGsrc2 + height2 * width2; // pBcomp points to B component array

	// Pointers to the RGB arrays of the destination image
	pRdest = pDstImage;
	pGdest = pRdest + height * width;
	pBdest = pGdest + height * width;

    // Calculation of the size of the results array
    // How many 256 bit packets fit in the array?
    int numberOfPackets = (imageSize * sizeof(data_t) / sizeof(__m256));
    // If is not a exact number we need to add one more packet
    if (((imageSize * sizeof(data_t)) % sizeof(__m256)) != 0) {
        numberOfPackets++;
    }
    std::cout << numberOfPackets << "\n";

    data_t *rSrc = (data_t *)_mm_malloc(sizeof(__m256) * numberOfPackets, sizeof(__m256));
    data_t *gSrc = (data_t *)_mm_malloc(sizeof(__m256) * numberOfPackets, sizeof(__m256));
    data_t *bSrc = (data_t *)_mm_malloc(sizeof(__m256) * numberOfPackets, sizeof(__m256));

    data_t *rSrc2 = (data_t *)_mm_malloc(sizeof(__m256) * numberOfPackets, sizeof(__m256));
    data_t *gSrc2 = (data_t *)_mm_malloc(sizeof(__m256) * numberOfPackets, sizeof(__m256));
    data_t *bSrc2 = (data_t *)_mm_malloc(sizeof(__m256) * numberOfPackets, sizeof(__m256));

    data_t *rDest = (data_t *)_mm_malloc(sizeof(__m256) * numberOfPackets, sizeof(__m256));
    data_t *gDest = (data_t *)_mm_malloc(sizeof(__m256) * numberOfPackets, sizeof(__m256));
    data_t *bDest = (data_t *)_mm_malloc(sizeof(__m256) * numberOfPackets, sizeof(__m256));

    ColorPointer srcImg = {rSrc, gSrc, bSrc};
    ColorPointer srcImg2 = {rSrc2, gSrc2, bSrc2};
    ColorPointer destImg = {rDest, gDest, bDest};
    // Initialize data 
    for (int i = 0; i < imageSize; i++)
    {
        *(rSrc + i) = *(pRsrc + i);
        *(gSrc + i) = *(pGsrc + i);
        *(bSrc + i) = *(pBsrc + i);

        *(rSrc2 + i) = *(pRsrc2 + i);
        *(gSrc2 + i) = *(pGsrc2 + i);
        *(bSrc2 + i) = *(pBsrc2 + i);

        *(rDest + i) = *(pRdest + i);
        *(gDest + i) = *(pGdest + i);
        *(bDest + i) = *(pBdest + i);
    }

    /*********************************************
	 * Algorithm start
	 *
	 * Measure initial time
	 *
	 */
    printf("Running task    : ");
    if (clock_gettime(CLOCK_REALTIME, &tStart))
    {
        exit(EXIT_FAILURE);
    }

    /************************************************
	 * Overlap algorithm between two source images into a destination image
	 */
    for (int j = 0; j < SAMPLES; j++)
    {
        destImg = applyOverlap(srcImg, srcImg2, numberOfPackets, destImg);
    }

    /***********************************************
	 * End of the algorithm
	 *
	 * Measure the final time and calculate the time spent
	 *
	 */
    if (clock_gettime(CLOCK_REALTIME, &tEnd))
    {
        exit(EXIT_FAILURE);
    }
     
    /* Save results obtained during overlap by intrinsincs and check for possible overflow */
    for (int i = 0; i < imageSize; i++)
    {
        *(pRdest + i) = checkOverflow(*(destImg.red + i));
        *(pGdest + i) = checkOverflow(*(destImg.green + i));
        *(pBdest + i) = checkOverflow(*(destImg.blue + i));
    }

    _mm_free(rSrc);
    _mm_free(gSrc);
    _mm_free(bSrc);
    _mm_free(rSrc2);
    _mm_free(gSrc2);
    _mm_free(bSrc2);
    _mm_free(rDest);
    _mm_free(gDest);
    _mm_free(bDest);

    dElapsedTimeS = (tEnd.tv_sec - tStart.tv_sec);
    dElapsedTimeS += (tEnd.tv_nsec - tStart.tv_nsec) / 1e+9;
    printf("Elapsed time    : %f s.\n", dElapsedTimeS);

    // Create a new image object with the calculated pixels
    // In case of normal color image use nComp=3,
    // In case of B&W image use nComp=1.
    CImg<data_t> dstImage(pDstImage, width, height, 1, nComp);
    // Store the destination image in disk
    dstImage.save(DESTINATION_IMG);

    // Display the destination image
    dstImage.display(); // If needed, show the result image

    return (0);
}

ColorPointer applyOverlap(ColorPointer srcImg, ColorPointer srcImg2, int numberOfPackets, ColorPointer destImg)
{
    const __m256 const_255 = _mm256_set1_ps(255);
    const __m256 const_2 = _mm256_set1_ps(2);
    __m256 srcR;
    __m256 srcG;
    __m256 srcB;
    __m256 srcR2;
    __m256 srcG2;
    __m256 srcB2;
    __m256 redDst;
    __m256 greenDst;
    __m256 blueDst;

    for(int i = 0; i < numberOfPackets; i++) 
    {
        /* Source images load into registers */
        srcR = _mm256_load_ps(srcImg.red + ITEMS_PER_PACKET * i);
        srcG = _mm256_load_ps(srcImg.green + ITEMS_PER_PACKET * i);
        srcB = _mm256_load_ps(srcImg.blue + ITEMS_PER_PACKET * i);

        srcR2 = _mm256_load_ps(srcImg2.red + ITEMS_PER_PACKET * i);
        srcG2 = _mm256_load_ps(srcImg2.green + ITEMS_PER_PACKET * i);
        srcB2 = _mm256_load_ps(srcImg2.blue + ITEMS_PER_PACKET * i);

        redDst = _mm256_sub_ps(const_255, srcR2);
        greenDst = _mm256_sub_ps(const_255, srcG2);
        blueDst = _mm256_sub_ps(const_255, srcB2);

        redDst = _mm256_mul_ps(redDst, srcR); 
        greenDst = _mm256_mul_ps(greenDst, srcG);
        blueDst = _mm256_mul_ps(blueDst, srcB);

        redDst = _mm256_mul_ps(redDst, const_2);
        greenDst = _mm256_mul_ps(greenDst, const_2);
        blueDst = _mm256_mul_ps(blueDst, const_2);

        redDst = _mm256_div_ps(redDst, const_255);
        greenDst = _mm256_div_ps(greenDst, const_255);
        blueDst = _mm256_div_ps(blueDst, const_255);

        redDst = _mm256_add_ps(redDst, srcR2);
        greenDst = _mm256_add_ps(greenDst, srcG2);
        blueDst = _mm256_add_ps(blueDst, srcB2);

        redDst = _mm256_mul_ps(redDst, srcR2);
        greenDst = _mm256_mul_ps(greenDst, srcG2);
        blueDst = _mm256_mul_ps(blueDst, srcB2);

        redDst = _mm256_div_ps(redDst, const_255);
        greenDst = _mm256_div_ps(greenDst, const_255);
        blueDst = _mm256_div_ps(blueDst, const_255);

        redDst = _mm256_min_ps(redDst, const_255);
        greenDst = _mm256_min_ps(greenDst, const_255);
        blueDst = _mm256_min_ps(blueDst, const_255);

        _mm256_store_ps(destImg.red + ITEMS_PER_PACKET * i, redDst);
        _mm256_store_ps(destImg.green + ITEMS_PER_PACKET * i, greenDst);
        _mm256_store_ps(destImg.blue + ITEMS_PER_PACKET * i, blueDst);
    }

    return destImg;
}

/**
 * Auxiliar method to prevent overflow during algorithm execution 
 */
data_t checkOverflow(data_t toCheck)
{
    if (toCheck > 255)
        return 255;
    else if (toCheck < 0)
        return 0;
    else
        return toCheck;
}